# BankApp

Test de desarrollo móvil Android

## Getting started

Tomando como base https://xd.adobe.com/view/3984e340-19f0-4fa8-89c1-f82f5b39607b-6cd7/specs/

Realizado el primer maquetado de la ventana principal.

### Observaciones

Al replicar siguiendo las especificaciones de fuente y tamaño de fuente aún en un teléfono de más de 6" varios de los textos se ven en dos líneas. Aunque me habría sido posible alterar el tamaño de la fuente decidí no hacerlo pues usualmente los diseños deben ser hasta donde sea posible como el depto de UI/UX indica.

En la ventana principal, varios de los componentes deben ser reemplazados por ReciclerView que le pasará la información en forma dinámica, en el diseño relaizado los deje como Cards directamente usando dos ScrollView uno para la parte superior u otro para los últimos movimientos.






